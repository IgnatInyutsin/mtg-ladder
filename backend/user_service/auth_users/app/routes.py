from django.urls import include, path
from rest_framework import routers
from .views import ActivationViewSet
from .views import UserViewSet

#устанавливаем пути
router = routers.DefaultRouter()
router.register("activate/(?P<uid>[\w.@+-]+)/(?P<token>[\w.@+-]+)", ActivationViewSet)
router.register("", UserViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls))
]