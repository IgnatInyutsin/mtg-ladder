from django.db import models
from django.contrib.auth.models import User
import uuid

# Модель общего профиля пользователя для расширения стандартной модели DJANGO
class Profile(models.Model):
    # ID профиля
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Объект пользователя
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # Описание профиля
    description = models.TextField(default="")

    def __str__(self):
        return "Profile of " + self.user.username

# Модель профиля стандарта "Пионер"
class PioneerProfile(models.Model):
    # Объект пользователя
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="pioneer")
    # Относительный ELO рейтинг игрока на данный момент
    elo = models.IntegerField(default=1500)
    # Максимальный ELO рейтинг за все время
    max_elo = models.IntegerField(default=1500)
    # Минимальный ELO рейтинг за все время
    min_elo = models.IntegerField(default=1500)

    def __str__(self):
        return "Pioneer Profile of " + self.user.username