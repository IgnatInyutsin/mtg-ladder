from rest_framework import serializers
from auth_users.app.models import *
from django.contrib.auth.models import User
from email.utils import parseaddr
from django.db import transaction

class PioneerProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PioneerProfile
        fields = ["id", "elo", "max_elo", "min_elo"]

class UserSerializer(serializers.HyperlinkedModelSerializer):
    description = serializers.CharField(source="profile.description")
    pioneer = PioneerProfileSerializer()
    class Meta:
        model = User
        fields = ["id",
                  "username",
                  "first_name",
                  "last_name",
                  "description",
                  "date_joined",
                  "pioneer"]

class RegistrationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["email", "username", "first_name", "last_name", "password"]
        extra_kwargs = {
            "email": {"required": True},
            "username": {"required": True},
            "first_name": {"required": True},
            "last_name": {"required": True},
            "password": {"required": True}
        }

    @transaction.atomic
    def create(self, validated_data):
        # при создании хэшируем пароль
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

    def validate_username(self, value):
        # Получаем всех пользователей с этим именем
        check_query = User.objects.filter(username=value)

        # Если есть хотя бы один - ошибка, при регистрации такого быть не должно
        if check_query.exists():
            raise serializers.ValidationError('A User with this name already exists.')
        # Иначе успешно
        return value

    def validate_email(self, value):
        # если email неправильного формата
        if not '@' in parseaddr(value)[1]:
            raise serializers.ValidationError('A email format is wrong.')

        # Получаем всех пользователей с этим email
        check_query = User.objects.filter(email=value)

        # Если есть хотя бы один - ошибка, при регистрации такого быть не должно
        if check_query.exists():
            raise serializers.ValidationError('A User with this email already exists.')

        # Иначе успешно
        return value