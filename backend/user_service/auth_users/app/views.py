from rest_framework import viewsets
from rest_framework import mixins
from auth_users.app.serializers import UserSerializer, RegistrationSerializer
from django.contrib.auth.models import User
import requests
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from djoser.views import UserViewSet as ParentUserViewSet
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny

# метод подтверждения регистрации по get
class ActivationViewSet(mixins.ListModelMixin,
                        viewsets.GenericViewSet):
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )

    @swagger_auto_schema(operation_description="Запрос активации аккаунта",
                        responses={200: "{detail: \"detail text\"}"})
    def list(self, request, token, uid):
        # автоматическое изменение протокола
        protocol = 'https://' if request.is_secure() else 'http://'
        # добавление основной части ссылки
        web_url = protocol + request.get_host() + (":8000" if request.get_host() == "0.0.0.0" or request.get_host() == "127.0.0.1" or request.get_host() == "localhost" else "")
        # добавление пути
        post_url = web_url + "/api/auth/users/activation/"
        # отправка запроса на активацию
        post_data = {'uid': uid, 'token': token}
        result = requests.post(post_url, data=post_data)
        if str(result.status_code)[0] == '2':
            return Response("Ваш аккаунт был успешно активирован!")
        elif result.json().get("detail", None) == "Stale token for given user.":
            return Response("Ваш аккаунт уже активирован")
        else:
            return Response("Произошла непредвиденная ошибка. Сообщите о ней нам, написав на почту iggyinyutsin@gmail.com")

# наследуемся от прошлого ViewSet для изменения лишь нескольких его методов
class UserViewSet(ParentUserViewSet):
    def list(self, request):
        # Кастомный queryset и page
        queryset = User.objects.all().order_by('date_joined')
        page = self.paginate_queryset(queryset)

        # Если пагинация не пустая
        if page is not None:
            serializer = UserSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        # Иначе обычный возврат
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)
