import uuid
from django.db import models
from multiselectfield import MultiSelectField

# перечень цветов
COLORS = (("B", "Black"),
          ("G", "Green"),
          ("R", "Red"),
          ("U", "Blue"),
          ("W", "White"))
# перечень типов
TYPES = (("Artifact", "Artifact"),
          ("Card", "Card"),
           ("Conspiracy", "Conspiracy"),
            ("Creature", "Creature"),
             ("Dragon", "Dragon"),
              ("Dungeon", "Dungeon"),
               ("Eaturecray", "Eaturecray"),
                ("Elemental", "Elemental"),
                 ("Elite", "Elite"),
                  ("Emblem", "Emblem"),
                   ("Enchantment", "Enchantment"),
                    ("Ever", "Ever"),
                     ("Goblin", "Goblin"),
                      ("Hero", "Hero"),
                       ("Instant", "Instant"),
                        ("Jaguar", "Jaguar"),
                         ("Knights", "Knights"),
                          ("Land", "Land"),
                           ("Phenomenon", "Phenomenon"),
                            ("Plane", "Plane"),
                             ("Planeswalker", "Planeswalker"),
                              ("Scariest", "Scariest"),
                               ("Scheme", "Scheme"),
                                ("See", "See"),
                                 ("Sorcery", "Sorcery"),
                                  ("Specter", "Specter"),
                                   ("Summon", "Summon"),
                                    ("Token", "Token"),
                                     ("Tribal", "Tribal"),
                                      ("Vanguard", "Vanguard"),
                                       ("Wolf", "Wolf"),
                                        ("You’ll", "You’ll"),
                                         ("instant", "instant"))

# Модель
class Card(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Имя карты
    name = models.TextField()
    # Мана стоимость карты, количество затрачиваемой маны
    mana_value = models.IntegerField(blank=True, null=True)
    # Мана стоимость карты, изображение стоимости карт спецсимволами
    mana_cost = models.CharField(max_length=256, blank=True, null=True)
    # Список цветов карты
    colors = MultiSelectField(choices=COLORS, blank=True, null=True)
    # Текст на карте
    text = models.TextField(blank=True, null=True)
    # Список типов карты
    types = MultiSelectField(choices=TYPES, blank=True, null=True)
    # Сила у карт существ
    power = models.CharField(max_length=256, blank=True, null=True)
    # Выносливость у карт существ
    toughness = models.CharField(max_length=256, blank=True, null=True)
    # Лояльность у карт мироходцев
    loyalty = models.CharField(max_length=256, blank=True, null=True)
    # Oracle ID для Scryfall API
    scryfall_id = models.CharField(max_length=256)
    # Текст карты на английском
    english_text = models.TextField(blank=True, null=True)
    # Имя карты на английском
    english_name = models.TextField(blank=True, null=True)

    def __str__(self):
        return  self.name