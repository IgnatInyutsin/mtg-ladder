from django.db import models
import uuid

# Правила для карт
class CardRule(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Дата принятия правила
    date = models.DateField()
    # Текст правила
    text = models.TextField()
    # Объект карты, на которую распространяется данное правило
    card = models.ForeignKey('Card',  on_delete=models.CASCADE, related_name="rules")

    def __str__(self):
        return self.card.name + " " + self.date.isoformat()