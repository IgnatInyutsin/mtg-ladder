from rest_framework.test import APITestCase
from restapi.management.commands.load_card_database import Command
from restapi.app.models import Card
from restapi.app.rules.models import CardRule

class LoadCardDatabaseTest(APITestCase):
    def test_creating_cards(self):
        # Проверяем, что выполнился без ошибок
        self.assertEqual(Command.handle(None), "End.")
        # Проверяем количество карт
        self.assertEqual(len(Card.objects.all()) > 8900, True)
        # Проверяем количество правил
        self.assertEqual(len( CardRule.objects.all() ) > 15000, True)
