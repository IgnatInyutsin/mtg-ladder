from rest_framework.test import APITestCase
from restapi.celery import pull_cards_to_deck
from redis import StrictRedis
from rq import Queue
import os
from restapi.management.commands.load_card_database import Command

class PullCardsToDeckTest(APITestCase):
    # тестовые данные
    tested_data = {
        "name": "Hidden Strings",
        "format": "Pioneer",
        "cards": [
            {
                "count": 4,
                "card": {"id": "176089df-410f-518e-8c05-d7a36255f389"}
            }
        ],
        "author": {"id": 1}
    }

    # правильные данные после теста
    true_data = {
        "name": "Hidden Strings",
        "format": "Pioneer",
        "cards": [
            {
                "count": 4,
                "card": {
                    "id": "176089df-410f-518e-8c05-d7a36255f389",
                    "name": "Визирь Перевернувшихся Часов",
                    "scryfall_id": "699fc7cf-1ad3-4c32-b3e4-49f9a2da4b26"
                }
            }
        ],
        "author": {"id": 1}
    }

    def setUp(self):
        # Добавляем в свою очередь
        os.environ["DECK_SERVICE_REDIS"] = "redis://card_service_redis:6379"
        # Загружаем базу данных
        Command.handle(None)

    def test_function(self):
        # проверяем, правильная ли инфа
        self.assertEqual(pull_cards_to_deck(self.tested_data), self.true_data)