from rest_framework.test import APITestCase
from rest_framework import status
from restapi.management.commands.load_card_database import Command

class SearchCardTest(APITestCase):
    def setUp(self):
        Command.handle(None)
    def test_search(self):
        # тест поиска по имени на русском
        request = self.client.get("/api/cards/?name=Тайные%20нити")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Тайные Нити")
        # тест поиска по частичке имени на русском
        request = self.client.get("/api/cards/?name=Тайные%20ни")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Тайные Нити")
        # тест поиска по  имени на английском
        request = self.client.get("/api/cards/?name=Hidden%20strings")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Тайные Нити")
        # тест поиска по частичке имени на английском
        request = self.client.get("/api/cards/?name=Hidden%20str")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Тайные Нити")
        # тест поиска по тексту
        request = self.client.get("/api/cards/?text=Вы%20можете%20повернуть%20или%20развернуть%20целевой%20перманент")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Тайные Нити")
        # тест поиска по мана-стоимости
        request = self.client.get("/api/cards/?mana_value_min=13&mana_value_max=13")
        self.assertEqual(request.status_code, status.HTTP_200_OK)
        self.assertEqual(request.data.get("results")[0].get("name"), "Эмракул, Обетованная Гибель")
