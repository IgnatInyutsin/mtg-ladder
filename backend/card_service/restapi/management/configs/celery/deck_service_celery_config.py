import os

BROKER_URL = os.environ.get("DECK_SERVICE_REDIS")
RESULT_BACKEND = os.environ.get("DECK_SERVICE_REDIS")