from rest_framework import permissions
import requests
from restapi.settings import USER_SERVICE

class AllowForAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        # проверяем токен на валидность
        r = requests.get(USER_SERVICE + "api/auth/users/me/",
                         headers={"Authorization": request.headers.get("Authorization"),
                                  "Host": "mtg.perpetum-mobile.ru"})

        # если валиден - разрешаем
        return r.status_code == 200