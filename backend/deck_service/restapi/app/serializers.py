from restapi.app.models import *
from restapi.app.author.models import *
from rest_framework import serializers
from restapi.app.docs import *

# Сериализатор для отдельной карточки
class CardSerializer(serializers.HyperlinkedModelSerializer):
    scryfall_id = ScryfallIdField()
    class Meta:
        model = Card
        fields = ["id",
                  "name",
                  "scryfall_id"]

# Сериализатор для отдельной карточки в POST
class CardPostSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.UUIDField()
    class Meta:
        model = Card
        fields = ["id"]

# Сериализатор группы карт
class CardsGroupSerializer(serializers.HyperlinkedModelSerializer):
    count = CountField()
    card = CardSerializer()

    class Meta:
        model = CardsPack
        fields = ['count',
                  'card']

# Сериализатор для группы карт в POST
class CardsGroupPostSerializer(serializers.HyperlinkedModelSerializer):
    count = CountField()
    card = CardPostSerializer()

    class Meta:
        model = CardsPack
        fields = ['count',
                  'card']

# Сериализатор автора
class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ["id",
                  "username"]

# Сериализатор для GET
class DeckSerializer(serializers.HyperlinkedModelSerializer):
    name = DeckNameField()
    format = DeckFormatField()
    created_data = CreatedDeckData()
    updated_data = UpdatedDeckData()
    author = AuthorSerializer()
    cards = CardsGroupSerializer(many=True)

    class Meta:
        model = Deck
        fields = ['id',
                  'name',
                  'format',
                  'created_data',
                  'updated_data',
                  'author',
                  'cards']

# Сериализатор для POST
class DeckPOSTSerializer(serializers.HyperlinkedModelSerializer):
    name = DeckNameField()
    format = DeckFormatField()
    cards = CardsGroupPostSerializer(many=True)

    class Meta:
        model = Deck
        fields = ['name',
                  'format',
                  'cards']