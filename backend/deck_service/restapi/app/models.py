import uuid
from django.db import models
from restapi.app.author.models import *

# Выбор форматов
FORMATS = [
    ("Pioneer", "Pioneer")
]

# Модель колоды, в которой содержится общая информация о колоде
class Deck(models.Model):
    id = models.UUIDField(primary_key=True)
    # Название колоды
    name = models.CharField(max_length=150)
    # Формат
    format = models.CharField(choices=FORMATS, max_length=150)
    # Дата создания
    created_data = models.DateTimeField(auto_now_add=True)
    # Дата обновления
    updated_data = models.DateTimeField(auto_now=True)
    # Ссылка на урезанную модель пользователя, который является автором колоды
    author = models.ForeignKey('Author', on_delete=models.CASCADE, related_name="decks")

# Модель наборов одинаковых карт в колодах
class CardsPack(models.Model):
    id = models.UUIDField(primary_key=True)
    # Ссылка на урезанную модель карты
    card = models.ForeignKey('Card', on_delete=models.CASCADE)
    # Ссылка на колоду, в которой находится эта карта
    deck = models.ForeignKey('Deck', on_delete=models.CASCADE, related_name="cards")
    # Количество карт в наборе
    count = models.IntegerField()

# Урезанная модель карты
class Card(models.Model):
    id = models.UUIDField(primary_key=True)
    # Имя карты
    name = models.TextField()
    # Oracle ID, используется в Scryfall API
    scryfall_id = models.UUIDField()