from rest_framework import viewsets
from rest_framework import mixins
from drf_yasg.utils import swagger_auto_schema
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from .models import Deck
from .serializers import *
from .permissions import AllowForAuthenticated
from rest_framework.serializers import ValidationError
import requests
from restapi.celery import get_cards_from_card_service
import uuid
from restapi.settings import USER_SERVICE
from restapi.celery import app
from rest_framework.response import Response


class DeckViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    pagination_class = PageNumberPagination
    queryset = Deck.objects.all().order_by('created_data')
    serializer_class = DeckSerializer
    permission_classes = (AllowAny,)

    def get_serializer_class(self):
        if self.request.method == "GET":
            return DeckSerializer
        else:
            return DeckPOSTSerializer

    def get_permissions(self):
        if self.request.method == "GET":
            return (AllowAny(),)
        else:
            return (AllowForAuthenticated(),)

    @swagger_auto_schema(operation_description="Получить список всех колод",
                         tags=["deck"])
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(operation_description="Получить конкретную колоду по ID",
                         tags=["deck"])
    def retrieve(self, request, pk):
        return super().retrieve(request, pk)

    @swagger_auto_schema(operation_description="Добавить колоду", tags=["deck"])
    def create(self, request, *args, **kwargs):
        # Список id базовых земель
        basic_lands_ids = [uuid.uuid5(uuid.NAMESPACE_DNS, "Болото"),
                           uuid.uuid5(uuid.NAMESPACE_DNS, "Лес"),
                           uuid.uuid5(uuid.NAMESPACE_DNS, "Равнина"),
                           uuid.uuid5(uuid.NAMESPACE_DNS, "Остров"),
                           uuid.uuid5(uuid.NAMESPACE_DNS, "Гора")]

        # Получаем наш сериализатор
        serializer = self.get_serializer(data=request.data)

        # Первичная валидация
        if not serializer.is_valid(raise_exception=True):
            raise ValidationError(serializer.errors)

        # Вторичная валидация
        for i in range(len(request.data["cards"])):
            if (request.data["cards"][i]["count"] > 4) and not (request.data["cards"][i]["card"]["id"] in basic_lands_ids):

                raise ValidationError(["Cards count may be only <4 if this isn't basic land"])

        # Получаем данные о пользователе
        r = requests.get(USER_SERVICE + "api/auth/users/me/",
                         headers={"Authorization": request.headers.get("Authorization", ""),
                                  "Host": "mtg.perpetum-mobile.ru"})

        # добавляем асинхронную задачу
        app.send_task("FirstStepOfCreatingDeck", (request.data, r.json()))
        return Response(status=202, data={"code": "TASK_IS_CREATING", "text": "You task add in queue"})