from rest_framework import viewsets
from rest_framework import mixins
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from restapi.app.models import *
from restapi.app.author.models import *
from restapi.app.author.serializers import *


class AuthorViewSet(mixins.RetrieveModelMixin,
                    viewsets.GenericViewSet):
    queryset = Deck.objects.all().order_by('-created_data')
    serializer_class = AuthorSerializer
    permission_classes = (AllowAny,)

    def retrieve(self, request, pk):
        return super().retrieve(request, pk)