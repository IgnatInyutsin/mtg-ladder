import uuid
from django.db import models

# Урезанная модель пользователя-автора
class Author(models.Model):
    id = models.UUIDField(primary_key=True)
    # ник пользователя
    username = models.TextField()