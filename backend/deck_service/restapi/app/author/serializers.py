from restapi.app.models import *
from restapi.app.author.models import *
from rest_framework import serializers
from restapi.app.docs import *
from restapi.app.docs import *

# Сериализатор автора
class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ["id",
                  "username",
                  "decks"]

# Сериализатор для GET
class DeckSerializer(serializers.HyperlinkedModelSerializer):
    name = DeckNameField()
    format = DeckFormatField()
    created_data = CreatedDeckData()
    updated_data = UpdatedDeckData()


    class Meta:
        model = Deck
        fields = ['id',
                  'name',
                  'format',
                  'created_data',
                  'updated_data']