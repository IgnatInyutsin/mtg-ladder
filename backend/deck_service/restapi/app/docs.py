from drf_yasg import openapi
from rest_framework import serializers

# Описание имени колоды в документации
class DeckNameField(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "DeckName",
            "description": "Имя колоды"
        }

# Формат для колоды в документации
class DeckFormatField(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "DeckFormat",
            "description": "Формат, в котором играет эта колода"
        }

# Дата создания колоды
class CreatedDeckData(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "DeckCreatedData",
            "description": "Дата создания колоды"
        }

# Дата обновления колоды
class UpdatedDeckData(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "DeckUpdatedData",
            "description": "Дата обновления колоды"
        }

# Поле родительской колоды
class CountField(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "CountOfCards",
            "description": "количество карт данного номинала"
        }

class ScryfallIdField(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "ScryfallOracleId",
            "description": "ScryfallOracleId для взаимодействия с ScryfallAPI"
        }