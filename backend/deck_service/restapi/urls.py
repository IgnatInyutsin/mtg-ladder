from django.contrib import admin
from django.urls import path, include
from .yasg import urlpatterns as doc_urls

urlpatterns = []

urlpatterns += doc_urls

urlpatterns += [
    path('api/decks/', include('restapi.app.routes')),
    path('api/decks/admin/', admin.site.urls),
    path('api/decks/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]