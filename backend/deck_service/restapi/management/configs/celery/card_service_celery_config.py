import os

BROKER_URL = os.environ.get("CARD_SERVICE_REDIS")
RESULT_BACKEND = os.environ.get("CARD_SERVICE_REDIS")