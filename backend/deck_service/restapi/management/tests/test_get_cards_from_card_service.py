from rest_framework.test import APITestCase
from restapi.celery import get_cards_from_card_service
from redis import StrictRedis
from rq import Queue
import os

class GetCardsFromCardServiceTest(APITestCase):
    # тестовые данные
    tested_data = {
        "name": "Hidden Strings",
        "format": "Pioneer",
        "cards": [
            {
                "count": 1,
                "card": {"id": "176089df-410f-518e-8c05-d7a36255f389"}
            }
        ]
    }
    user_tested_data = {
        "id": 1,
        "username": "ignat",
        "first_name": "Ignat",
        "last_name": "Inyutsin",
        "description": "",
        "date_joined": "2022-08-24T05:49:39.378895Z",
        "pioneer": {
            "id": 1,
            "elo": 1500,
            "max_elo": 1500,
            "min_elo": 1500
        }
    }

    # правильные данные после теста
    true_data = {
        "name": "Hidden Strings",
        "format": "Pioneer",
        "cards": [
            {
                "count": 1,
                "card": {"id": "176089df-410f-518e-8c05-d7a36255f389"}
            }
        ],
        "author": {"id": 1}
    }

    def setUp(self):
        # Добавляем в свою очередь
        os.environ["CARD_SERVICE_REDIS"] = "redis://deck_service_redis:6379"

    def test_function(self):
        # проверяем, правильная ли инфа
        self.assertEqual(get_cards_from_card_service(self.tested_data, self.user_tested_data), self.true_data)