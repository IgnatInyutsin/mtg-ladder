from rest_framework.test import APITestCase
from restapi.celery import create_deck
import os
from restapi.app.models import *

class CreateDeckTest(APITestCase):
    # тестовые данные
    tested_data = {
        "name": "Hidden Strings",
        "format": "Pioneer",
        "cards": [
            {
                "count": 4,
                "card": {
                    "id": "176089df-410f-518e-8c05-d7a36255f389",
                    "name": "Визирь Перевернувшихся Часов",
                    "scryfall_id": "699fc7cf-1ad3-4c32-b3e4-49f9a2da4b26"
                }
            }
        ],
        "author": {"id": 1}
    }

    def setUp(self):
        # Добавляем в свою очередь
        Author.objects.create(id=1, username="test")

    def test_function_without_card_creating(self):
        create_deck(self.tested_data)

        self.assertEqual(len(Deck.objects.all()) == 1, True)
        self.assertEqual(len(CardsPack.objects.all()) == 1, True)
        self.assertEqual(len(Card.objects.all()) == 1, True)

        self.assertEqual(Deck.objects.get(name="Hidden Strings").format == "Pioneer", True)
        self.assertEqual(CardsPack.objects.all()[0].count == 4, True)
        self.assertEqual(Card.objects.get(id="176089df-410f-518e-8c05-d7a36255f389").name == "Визирь Перевернувшихся Часов", True)

    def test_function_with_card_creating(self):
        Card.objects.create(id="176089df-410f-518e-8c05-d7a36255f389",
                            name="Визирь Перевернувшихся Часов",
                            scryfall_id="699fc7cf-1ad3-4c32-b3e4-49f9a2da4b26")

        create_deck(self.tested_data)

        self.assertEqual(len(Deck.objects.all()) == 1, True)
        self.assertEqual(len(CardsPack.objects.all()) == 1, True)
        self.assertEqual(len(Card.objects.all()) == 1, True)

        self.assertEqual(Deck.objects.get(name="Hidden Strings").format == "Pioneer", True)
        self.assertEqual(CardsPack.objects.all()[0].count == 4, True)
        self.assertEqual(Card.objects.get(id="176089df-410f-518e-8c05-d7a36255f389").name == "Визирь Перевернувшихся Часов", True)