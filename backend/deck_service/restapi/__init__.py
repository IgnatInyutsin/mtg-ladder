from __future__ import absolute_import, unicode_literals

import os,django

os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "restapi.settings") # project_name название проекта
django.setup()

# Это позволит убедиться, что приложение всегда импортируется, когда запускается Django
from .celery import app as celery_app

__all__ = ('celery_app',)
