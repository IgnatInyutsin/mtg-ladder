"""
Файл настроек Celery
https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html
"""
from __future__ import absolute_import
import os
from celery import Celery
from redis import StrictRedis
from rq import Queue
from restapi.app.models import *
from django.db import transaction
from uuid import UUID, uuid4

# он установит модуль настроек по умолчанию Django для приложения 'celery'.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'restapi.settings')

app = Celery("restapi")

# Для получения настроек Django, связываем префикс "CELERY" с настройкой celery
app.config_from_object('django.conf:settings', namespace='CELERY')

# загрузка tasks.py в приложение django
app.autodiscover_tasks()

# первый этап создания колоды
@transaction.atomic
@app.task(name="FirstStepOfCreatingDeck", task_id="FirstStepOfCreatingDeck")
def get_cards_from_card_service(request_data, user):
    # Подключение к чужому Celery
    card_celery = Celery()
    card_celery.config_from_object("restapi.management.configs.celery.card_service_celery_config")

    # создаем автора
    if len(Author.objects.filter(id=user["id"])) == 0:
        Author.objects.create(id=user["id"], username=user["username"])

    # Добавляем информацию о нем в конечную информацию
    request_data["author"]= { "id": user["id"] }

    # добавляем в очередь таску
    card_celery.send_task("SecondStepOfCreatingDeck", [request_data, ])

    return request_data

# последний этап создания колоды
@transaction.atomic
@app.task(name="ThirdStepOfCreatingDeck", task_id="ThirdStepOfCreatingDeck")
def create_deck(request_data):
    # находим автора
    author = Author.objects.get(id=request_data["author"]["id"])
    # Создаем колоду
    deck = Deck(id=uuid4(),
                name=request_data["name"],
                format=request_data["format"],
                author=author)
    deck.save()

    # Создаем карты
    for i in range(len(request_data["cards"])):
        # если не существует карты - создаем
        if len(Card.objects.filter(id=request_data["cards"][i]["card"]["id"])) < 1:
            card = Card(id=UUID(request_data["cards"][i]["card"]["id"]),
                        name=request_data["cards"][i]["card"]["name"],
                        scryfall_id=request_data["cards"][i]["card"]["scryfall_id"])
            card.save()
        else:
            card = Card.objects.get(id=request_data["cards"][i]["card"]["id"])

        # Создаем пак карт
        CardsPack.objects.create(id=uuid4(),
                                 card=card,
                                 deck=deck,
                                 count=int(request_data["cards"][i]["count"]))