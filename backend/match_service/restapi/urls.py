from django.contrib import admin
from django.urls import path, include
from .yasg import urlpatterns as doc_urls

urlpatterns = []

urlpatterns += doc_urls

urlpatterns += [
    path('api/matches/admin/', admin.site.urls),
    path('api/matches/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]