from drf_yasg import openapi
from rest_framework import serializers

# Описание имени колоды в документации
class RatingField(serializers.IntegerField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_INTEGER,
            "title": "MatchRating",
            "description": "Количество очков Elo, которые получил первый игрок и потерял второй"
        }

# Описание имени колоды в документации
class WinnerCommentField(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "MatchWinnerComment",
            "description": "Комментарий победителя"
        }

# Дата создания колоды
class CreatedMatchData(serializers.CharField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_STRING,
            "title": "MatchCreatedData",
            "description": "Дата сыгранного матча"
        }