import uuid
from django.db import models
from multiselectfield import MultiSelectField

# Модель игрока
class Player(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField()

# Модель профиля формата Pioneer
class PioneerProfile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    elo = models.IntegerField(default=1500)
    # Объект пользователя
    user = models.OneToOneField(Player, on_delete=models.CASCADE, related_name="pioneer")

# Модель колоды
class Deck(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField()

# Модель матча
class Match(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="win_matches")
    second_player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="lose_matches")
    first_player_deck = models.ForeignKey(Deck, on_delete=models.CASCADE, related_name="win_matches")
    second_player_deck = models.ForeignKey(Deck, on_delete=models.CASCADE, related_name="lose_matches")
    rating = models.IntegerField()
    winner_comment = models.TextField()
    created_data = models.DateTimeField(auto_now_add=True)
    format = models.TextField()

    def __str__(self):
        return self.first_player.name + " vs " + self.second_player.name + " " + self.id

# Модель репорта на матч
class Report(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField()
    description = models.TextField()
    match = models.ForeignKey(Match, on_delete=models.CASCADE, related_name="reports")
    created_data = models.DateTimeField(auto_now_add=True)

# Модель сообщения в репорте матча
class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_data = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Player, on_delete=models.CASCADE, related_name="messages")
    text = models.TextField()
    report = models.ForeignKey(Report, on_delete=models.CASCADE, related_name="messages")