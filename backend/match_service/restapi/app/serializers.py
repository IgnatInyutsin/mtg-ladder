from restapi.app.models import *
from rest_framework import serializers
from restapi.app.docs import *

# Сериализатор профиля формата Пионер
class PioneerField(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PioneerProfile
        fields = ["elo"]

# Сериализатор игрока
class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    pioneer = PioneerSerializer()
    class Meta:
        model = Player
        fields = ["id",
                  "name",
                  "pioneer"]

# Сериализатор игрока POST
class PlayerPostSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.UUIDField()
    class Meta:
        model = Player
        fields = ["id"]

# Сериализатор колоды POST
class DeckPostSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.UUIDField()
    class Meta:
        model = Deck
        fields = ["id"]

# Сериализатор колоды
class DeckSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Deck
        fields = ["id",
                  "name"]

# Сериализатор репорта
class ReportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Report
        fields = ["id",
                  "title",
                  "description",
                  "created_data"]

# Сериализатор для GET
class PioneerMatchSerializer(serializers.HyperlinkedModelSerializer):
    first_player = PlayerSerializer()
    second_player = PlayerSerializer()
    rating = RatingField()
    winner_comment = WinnerCommentField()
    created_data = CreateMatchField()
    reports = ReportSerializer()
    first_player_deck = DeckSerializer()
    second_player_deck = DeckSerializer()

    class Meta:
        model = Match
        fields = ["id",
                  "first_player",
                  "second_player",
                  "first_player_deck",
                  "second_player_deck"
                  "rating",
                  "winner_comment",
                  "created_data",
                  "reports"]

# Сериализатор для POST
class PioneerMatchPostSerializer(serializers.HyperlinkedModelSerializer):
    first_player = PlayerPostSerializer()
    second_player = PlayerPostSerializer()
    first_player_deck = DeckPostSerializer()
    second_player_deck = DeckPostSerializer()
    winner_comment = WinnerCommentField()

    class Meta:
        model = Match
        fields = ["id",
                  "first_player",
                  "second_player",
                  "first_player_deck",
                  "second_player_deck"
                  "winner_comment"]

