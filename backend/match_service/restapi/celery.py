"""
Файл настроек Celery
https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html
"""
from __future__ import absolute_import
import os
from celery import Celery
from django.db import transaction
from redis import StrictRedis
from rq import Queue
from uuid import UUID

# он установит модуль настроек по умолчанию Django для приложения 'celery'.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'restapi.settings')

app = Celery("restapi")

# Для получения настроек Django, связываем префикс "CELERY" с настройкой celery
app.config_from_object('django.conf:settings', namespace='CELERY')

# загрузка tasks.py в приложение django
app.autodiscover_tasks()

# добавляем карты в колоду
@app.task(name="SecondStepOfCreatingDeck", task_id="SecondStepOfCreatingDeck")
def pull_cards_to_deck(request_data):
    # собираем список id
    cards_ids = [request_data["cards"][i]["card"]["id"]
                 for i in range(len(request_data["cards"]))]
    # Получаем список объектов, которые имеют это id
    cards = Card.objects.filter(id__in=cards_ids)

    # заполняем json
    for i in range(len(request_data["cards"])):
        card_id = request_data["cards"][i]["card"]["id"]
        current_card = None

        # ищем в полученном списке нужную нам карту
        for card in range(len(cards)):
            if str(cards[card].id) == card_id:
                current_card = cards[card]
                break

        request_data["cards"][i]["card"]["name"] = current_card.name
        request_data["cards"][i]["card"]["scryfall_id"] = current_card.scryfall_id

    ### Возвращаем ее в брокер сервиса создания колод
    # Подключение к чужому Celery
    deck_celery = Celery()
    deck_celery.config_from_object("restapi.management.configs.celery.deck_service_celery_config")

    # добавляем в очередь таску
    deck_celery.send_task("ThirdStepOfCreatingDeck", [request_data,])

    return request_data