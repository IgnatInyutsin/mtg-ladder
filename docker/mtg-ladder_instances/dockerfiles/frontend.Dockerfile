FROM nginx:latest
COPY ./frontend/dist/frontend/ /usr/share/nginx/html/
COPY ./docker/mtg-ladder_instances/dockerfiles/configs/nginx.conf /etc/nginx/conf.d/nginx.conf
