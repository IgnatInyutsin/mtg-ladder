import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";
import {RestApiConnector} from "../../assets/connectors/restapi";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  restapi = new RestApiConnector();
  session = ""

  constructor(private http: HttpClient, private cookies: CookieService) { }

  ngOnInit(): void {
    // получаем сессию
    this.session = this.cookies.get("session");

    // проверяем сессию
    if (this.cookies.get('session') != "") {
      this.http.get(this.restapi.restapiUrl + "auth/users/me/", {
        headers: new HttpHeaders({'Authorization': 'Token ' + this.cookies.get('session')})
      })
        .subscribe((data: any) => {}, (error: any) => {
          this.cookies.delete('session');
          location.reload();
        })
    }
  }

}
