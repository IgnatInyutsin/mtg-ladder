import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RestApiConnector} from "../../assets/connectors/restapi";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  restapi = new RestApiConnector();
  form = {
    email: "",
    password: ""
  };
  FAIL_LOGIN = false;

  constructor(private http: HttpClient, private cookies: CookieService, private router: Router) { }

  ngOnInit(): void {
    if (this.cookies.get("session") != "") {
      this.router.navigate(['/'])
    }
  }

  login(): void {
    // обнуляем не состоявшиеся входы
    this.FAIL_LOGIN = false;
    console.log(this.form)

    this.http.post(this.restapi.restapiUrl + "auth/token/login/", this.form).subscribe((data: any) => { //если успешно
      console.log(data)
      this.cookies.set('session', data.auth_token);
      location.reload();
    }, (error: any) => {// если ошибка
      console.log(error)
      this.FAIL_LOGIN = true;
    })
  }

}
