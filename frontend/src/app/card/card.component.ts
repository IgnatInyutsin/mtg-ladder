import {Input, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RestApiConnector} from "../../assets/connectors/restapi";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CardComponent implements OnInit {
  @Input() name: string = "";
  @Input() text: string = "";
  @Input() imgUrl: string = "";
  @Input() usdPrice: string = "";
  @Input() eurPrice: string = "";
  @Input() tixPrice: string = "";
  @Input() manyFaces: boolean = false;
  @Input() secondImgUrl: string = '';
  @Input() secondText: string = '';
  @Input() types: string | string[] = [];
  @Input() manaCost: string | undefined = "";
  @Input() power: string | undefined = "";
  @Input() toughness: string | undefined = "";
  @Input() loyalty: string | undefined = "";
  @Input() id: string = "";
  firstFace: boolean = true;
  rules:any[] = [];
  rest: RestApiConnector = new RestApiConnector();

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  toggleFace(): void {
    if (!this.firstFace) this.firstFace = true;
    else this.firstFace = false;
  }

  getRules(): void {
    // собираем правила
    this.http.get(this.rest.restapiUrl + "cards/" + this.id + "/rules/").subscribe((data:any) => {
      // сбор данных с scryfall api
      if (data.length < 1) this.rules = [{date: "СПЕЦИАЛЬНЫХ ПРАВИЛ", text: "НЕТ"}];
      else this.rules = data;
    });
  }

}
