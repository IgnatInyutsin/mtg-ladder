import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {RestApiConnector} from "../../assets/connectors/restapi";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  restapi = new RestApiConnector();

  constructor(private http: HttpClient, private cookies: CookieService, private router: Router) { }

  ngOnInit(): void {
    // если нет токена - редирект на главную, иначе - разлогин
    if (this.cookies.get('session') == '') {
      this.router.navigate(['/'])
    } else {
      this.http.post(this.restapi.restapiUrl + "auth/token/logout", {}, {
        headers: new HttpHeaders({
          'Authorization': 'Token ' + this.cookies.get('session')
        })
      }).subscribe((data: any) => {
        this.cookies.delete('session');
        location.reload();
      });
    }
  }

}
