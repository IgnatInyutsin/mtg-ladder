import { Component, OnInit } from '@angular/core';
import {isEmpty} from "rxjs";
import {RestApiConnector} from "../../assets/connectors/restapi";
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs/operators";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  restapi = new RestApiConnector();
  isRegistrated = false;

  warnings = {
    requiredNickname: false,
    requiredFirstName: false,
    requiredLastName: false,
    requiredEmail: false,
    requiredPassword: false,
    retryPasswordIncorrect: false,
    uniqueNickname: false,
    uniqueEmail: false,
    incorrectEmail: false
  }

  form = {
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    retryPassword: ""
  }

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  putForm(): void {
    // убираем все предупреждения до этого
    this.warnings.requiredNickname = false;
    this.warnings.requiredFirstName = false;
    this.warnings.requiredLastName = false;
    this.warnings.requiredEmail = false;
    this.warnings.requiredPassword = false;
    this.warnings.retryPasswordIncorrect = false;
    this.warnings.uniqueNickname = false;
    this.warnings.uniqueEmail = false;
    this.warnings.incorrectEmail = false;

    // первые валидации
    if (this.form.username == "")  this.warnings.requiredNickname = true;
    else if (this.form.first_name == "") this.warnings.requiredFirstName = true;
    else if (this.form.last_name == "") this.warnings.requiredLastName = true;
    else if (this.form.email == "") this.warnings.requiredEmail = true;
    else if (this.form.password == "") this.warnings.requiredPassword= true;
    else if (this.form.password != this.form.retryPassword) {
      this.warnings.retryPasswordIncorrect = true;
      this.form.retryPassword = "";
    }
    else if (!this.validateEmail(this.form.email)) this.warnings.incorrectEmail = true;
    // если все прошло - посылаем запрос
    else {
      this.http.post(this.restapi.restapiUrl + "auth/users/", this.form).subscribe((data: any) => { // если успешнр
        this.isRegistrated = true;
      }, (error: any) => {
        // вторая валидация
        if (error.error.username != undefined) this.warnings.uniqueNickname = true;
        if (error.error.email != undefined) this.warnings.uniqueEmail = true;
      }, )
    }

  }

  validateEmail(email: any): boolean {
    return email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
  }

}
